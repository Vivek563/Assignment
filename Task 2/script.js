
      const card = document.querySelector(".card");
      const loading = document.querySelector(".loading");
      let no_of_images = 25;

      function renderImages() {
        for (let i = 0; i < no_of_images; i++) {
          fetch("https://source.unsplash.com/random")
          .then((res) => {
            let img = document.createElement("img");
            img.src = res.url;
            card.appendChild(img);
          });
        }
      }

      window.onscroll = () => {
        const { scrollTop, clientHeight, scrollHeight } =
          document.documentElement;
        if (scrollTop + clientHeight + 1 >= scrollHeight) {
         
          loading.classList.add("show");
          renderImages();
        } else {
          loading.classList.remove("show");
        }
      };
 